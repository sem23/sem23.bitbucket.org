var searchData=
[
  ['ok',['ok',['../structros_node_handle.html#affd168f585739776b5f5c5c97b6dd895',1,'rosNodeHandle']]],
  ['osdelayuntil',['osDelayUntil',['../ros__os_8h.html#af3d64e69218dfb19cc49f1b7fcb322f5',1,'osDelayUntil(uint32_t *last_wake_time, uint32_t time_increment):&#160;ros_FreeRTOS.c'],['../ros___o_s___t_e_m_p_l_a_t_e_8c.html#af3d64e69218dfb19cc49f1b7fcb322f5',1,'osDelayUntil(uint32_t *last_wake_time, uint32_t time_increment):&#160;ros_OS_TEMPLATE.c']]],
  ['osinit',['osInit',['../ros__os_8h.html#af5b509c29695beecf0e803c121a0a310',1,'osInit():&#160;ros_FreeRTOS.c'],['../ros___o_s___t_e_m_p_l_a_t_e_8c.html#af5b509c29695beecf0e803c121a0a310',1,'osInit():&#160;ros_OS_TEMPLATE.c']]],
  ['oslockcritical',['osLockCritical',['../ros__os_8h.html#a0caa01d3e3908455e1aaf7afaa9396a7',1,'osLockCritical():&#160;ros_FreeRTOS.c'],['../ros___o_s___t_e_m_p_l_a_t_e_8c.html#a0caa01d3e3908455e1aaf7afaa9396a7',1,'osLockCritical():&#160;ros_OS_TEMPLATE.c']]],
  ['osmsleep',['osMSleep',['../ros__os_8h.html#ad89889d7fe531282e649064388db14b4',1,'osMSleep(uint32_t ms):&#160;ros_FreeRTOS.c'],['../ros___o_s___t_e_m_p_l_a_t_e_8c.html#ad89889d7fe531282e649064388db14b4',1,'osMSleep(uint32_t ms):&#160;ros_OS_TEMPLATE.c']]],
  ['osrosnode',['osROSNode',['../structos_r_o_s_node.html',1,'']]],
  ['osrosnode_5ft',['osROSNode_t',['../ros__types_8h.html#a01d43732737f2da0c06cf1a6a910eff8',1,'ros_types.h']]],
  ['osstartnode',['osStartNode',['../ros__os_8h.html#a8b2411cc6389370a9746b582e0acf020',1,'osStartNode(osROSNode_t *node):&#160;ros_FreeRTOS.c'],['../ros___o_s___t_e_m_p_l_a_t_e_8c.html#a8b2411cc6389370a9746b582e0acf020',1,'osStartNode(osROSNode_t *node):&#160;ros_OS_TEMPLATE.c']]],
  ['osunlockcritical',['osUnlockCritical',['../ros__os_8h.html#a19ef2a217ede69360cc3371eb22940eb',1,'osUnlockCritical():&#160;ros_FreeRTOS.c'],['../ros___o_s___t_e_m_p_l_a_t_e_8c.html#a19ef2a217ede69360cc3371eb22940eb',1,'osUnlockCritical():&#160;ros_OS_TEMPLATE.c']]],
  ['osusleep',['osUSleep',['../ros__os_8h.html#a71524465e6e7d5aa8a7ff50b89d4c0f0',1,'osUSleep(uint32_t us):&#160;ros_OS_TEMPLATE.c'],['../ros___o_s___t_e_m_p_l_a_t_e_8c.html#a71524465e6e7d5aa8a7ff50b89d4c0f0',1,'osUSleep(uint32_t us):&#160;ros_OS_TEMPLATE.c']]]
];
