var structros_packet =
[
    [ "buffer", "structros_packet.html#a56ed84df35de10bdb65e72b184309497", null ],
    [ "buffer_size", "structros_packet.html#adae708b20ac4fe01990712c8041e560a", null ],
    [ "checksum", "structros_packet.html#a59eac9627282a484fbaf0aa7aa3b8a9a", null ],
    [ "message_length", "structros_packet.html#aed3bb240f12548ae7b1cdd3cb931f1f8", null ],
    [ "message_length_", "structros_packet.html#ad34096b594fabf24ed404b2d64676770", null ],
    [ "sync_flag", "structros_packet.html#a6ae2fedbddb8800366308707a81375d7", null ],
    [ "sync_flag_", "structros_packet.html#afa4a918f1d48f68c7019c7b279a8b62f", null ],
    [ "topic_id", "structros_packet.html#ad562f54acc5597130e0710c356963dff", null ],
    [ "topic_id_", "structros_packet.html#a658133072b3c7b6eec860948c161c72a", null ]
];