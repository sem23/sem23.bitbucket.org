var structros_message_handle =
[
    [ "buffer_size", "structros_message_handle.html#adae708b20ac4fe01990712c8041e560a", null ],
    [ "deserializer", "structros_message_handle.html#a12422bdc2fb298b5ddc2d0026e8dc9b5", null ],
    [ "id", "structros_message_handle.html#a4fc3a0c58dfbd1e68224521185cb9384", null ],
    [ "md5sum", "structros_message_handle.html#a5adf459c9bbd0c80bd836a0801a416c3", null ],
    [ "nodehandle", "structros_message_handle.html#ac7651fc00f11451dd327d08b7cf7975c", null ],
    [ "serializer", "structros_message_handle.html#a7a9672bd84cb20fc66194094d3d43d91", null ],
    [ "sizer", "structros_message_handle.html#a0b0e1208cdbb95de7024a0b457a49dfe", null ],
    [ "topic", "structros_message_handle.html#affecb48e716753e10b44feac31f12529", null ],
    [ "type", "structros_message_handle.html#a23506fc4821ab6d9671f3e6222591a96", null ]
];